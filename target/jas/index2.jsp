<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<title>AO JAS</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-12">
				<h2>Het opslaan van... een brooddoos dmv jQuery & ajax</h2>
			</div>
		</div>
		<br /> <br /> <br />

		<div class="row">
			<div class="col-12">
				<h3>Nieuwe brooddoos toevoegen</h3>

				<div class="form-group row">
					<label class="col-sm-4">Naam doos:</label>
					<div class="col-sm-8">
						<s:textfield class="form-control" name="name" id="name"></s:textfield>
					</div>
				</div>
				<button class="btn btn-primary" id="savebrooddoos">Opslaan
				</button>
			</div>
		</div>

	</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

</body>
</html>