package nl.delphinity.jas.controller;

import java.util.HashSet;
import java.util.Set;

import com.opensymphony.xwork2.ActionSupport;

import nl.delphinity.jas.domain.Brood;
import nl.delphinity.jas.domain.Brooddoos;
import nl.delphinity.jas.persistence.service.BroodService;
import nl.delphinity.jas.persistence.service.BrooddoosService;

public class BrooddoosController extends ActionSupport{
	
	public Set<Brood> inhoudbrooddoos = new HashSet<Brood>();
	
	private BrooddoosService brooddoosservice = BrooddoosService.getInstance();
	private BroodService broodservice = BroodService.getInstance();
	
	public String execute() {
		inhoudbrooddoos.addAll(broodservice.readAll());
		System.out.println("hoi" + inhoudbrooddoos);
		return SUCCESS;
	}

	public String save() {
		
		System.out.println();
		Brooddoos b = new Brooddoos();
		b.setName("a");
		System.out.println(b);
		try {
			brooddoosservice.createBrooddoos(b);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return SUCCESS;
	}

	public Set<Brood> getInhoudbrooddoos() {
		return inhoudbrooddoos;
	}

	public void setInhoudbrooddoos(Set<Brood> inhoudbrooddoos) {
		this.inhoudbrooddoos = inhoudbrooddoos;
	}
	
	



}
