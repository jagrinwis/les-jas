package nl.delphinity.jas.persistence.service;

import java.util.Collection;
import java.util.Set;

import com.opensymphony.xwork2.ActionSupport;

import nl.delphinity.jas.domain.Brooddoos;
import nl.delphinity.jas.persistence.factory.DAOFactory;

public class BrooddoosService extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static BrooddoosService instance = null;

	public static BrooddoosService getInstance() {
		if (instance == null) {
			instance = new BrooddoosService();
		}
		return instance;
	}

	public static void setInstance(BrooddoosService instance) {
		BrooddoosService.instance = instance;
	}

	public void createBrooddoos(Brooddoos t) throws Exception {
		DAOFactory.getTheFactory().getBrooddoosDAO().saveOrUpdate(t);

	}
	
	public void subscribeToBrooddoos(Brooddoos t) {
		DAOFactory.getTheFactory().getBrooddoosDAO().saveOrUpdate(t);
	}

	public void deleteBrooddoos(Brooddoos t) {

		DAOFactory.getTheFactory().getBrooddoosDAO().delete(t);

	}
	public Brooddoos findBrooddoosById(int Id) {

		return DAOFactory.getTheFactory().getBrooddoosDAO().findById(Id);

	}

	public void updateBrooddoos(Brooddoos t) throws Exception {

		DAOFactory.getTheFactory().getBrooddoosDAO().saveOrUpdate(t);

	}

	public Collection<Brooddoos> readAllBrooddooss() {

		return DAOFactory.getTheFactory().getBrooddoosDAO().findAll();

	}
	public Set<Brooddoos> readAll() {

		return DAOFactory.getTheFactory().getBrooddoosDAO().findAll();

	}

}