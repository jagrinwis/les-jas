package nl.delphinity.jas.persistence.factory;

import org.hibernate.Session;

import nl.delphinity.jas.domain.Brooddoos;
import nl.delphinity.jas.domain.Brood;
import nl.delphinity.jas.persistence.dao.GenericHibernateDAO;
import nl.delphinity.jas.persistence.dao.BrooddoosDAO;
import nl.delphinity.jas.persistence.dao.BroodDAO;
import nl.delphinity.jas.persistence.interfaces.IBrooddoosDAO;
import nl.delphinity.jas.persistence.interfaces.IBroodDAO;
import nl.delphinity.jas.domain.Beleg;
import nl.delphinity.jas.persistence.dao.BelegDAO;
import nl.delphinity.jas.persistence.dao.GenericHibernateDAO;
import nl.delphinity.jas.persistence.interfaces.IBelegDAO;
import nl.delphinity.jas.persistence.utils.HibernateSessionManager;

public class HibernateDAOfactory extends DAOFactory {

	protected Session getCurrentSession() {
		return HibernateSessionManager.getSessionFactory().openSession();
	}

	@Override
	public IBrooddoosDAO getBrooddoosDAO() {
		// TODO Auto-generated method stub
		GenericHibernateDAO<Brooddoos, Integer> dao = null;
		try {
			dao = BrooddoosDAO.class.newInstance();
			dao.setSession(getCurrentSession());
		}catch (InstantiationException e) {
			e.printStackTrace();
		}catch(IllegalAccessException e) {
			e.printStackTrace();
		}
		return (IBrooddoosDAO) dao;
	}
	
	@Override
	public IBroodDAO getBroodDAO() {
		// TODO Auto-generated method stub
		GenericHibernateDAO<Brood, Integer> dao = null;
		try {
			dao = BroodDAO.class.newInstance();
			dao.setSession(getCurrentSession());
		}catch (InstantiationException e) {
			e.printStackTrace();
		}catch(IllegalAccessException e) {
			e.printStackTrace();
		}
		return (IBroodDAO) dao;
	}
		
	@Override
	public IBelegDAO getBelegDAO() {
		// TODO Auto-generated method stub
		GenericHibernateDAO<Beleg, Integer> dao = null;
		try {
			dao = BelegDAO.class.newInstance();
			dao.setSession(getCurrentSession());
		}catch (InstantiationException e) {
			e.printStackTrace();
		}catch(IllegalAccessException e) {
			e.printStackTrace();
		}
		return (IBelegDAO) dao;
	}

}