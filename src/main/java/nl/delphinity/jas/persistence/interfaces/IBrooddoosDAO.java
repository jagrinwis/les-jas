package nl.delphinity.jas.persistence.interfaces;

import nl.delphinity.jas.domain.Brooddoos;

public interface IBrooddoosDAO extends IGenericDAO<Brooddoos, Integer> {

}