package nl.delphinity.jas.persistence.interfaces;

import nl.delphinity.jas.domain.Brood;

public interface IBroodDAO extends IGenericDAO<Brood, Integer> {

}