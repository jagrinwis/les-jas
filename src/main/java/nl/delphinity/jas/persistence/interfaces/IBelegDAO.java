package nl.delphinity.jas.persistence.interfaces;

import nl.delphinity.jas.domain.Beleg;

public interface IBelegDAO extends IGenericDAO<Beleg, Integer> {

}